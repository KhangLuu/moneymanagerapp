import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { StackNavigator } from "react-navigation";
import HomeScreen from "./components/HomeScreen";

export default class App extends Component {
  render() {
    return <AppStackNavigator />;
  }
}
AppStackNavigator = StackNavigator({
  Main: {
    screen: HomeScreen
  }
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
