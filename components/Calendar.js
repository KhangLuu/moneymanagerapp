import React, { Component } from "react";
import { Text, StyleSheet, View, FlatList, ScrollView } from "react-native";
import { Calendar } from "react-native-calendars";
import {
  sumDayMoneyValueIN,
  sumDayMoneyValueOUT
} from "../databases/allSchemas";
import Icon from "react-native-vector-icons/FontAwesome";
import Dialog, {
  DialogContent,
  ScaleAnimation,
  DialogButton,
  DialogTitle
} from "react-native-popup-dialog";
import PieChart from "react-native-pie-chart";
import ActionButton from "react-native-action-button";

export default class moneyCalender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scaleAnimationDialog: true,
      day: 0,
      month: 0,
      year: 0,
      sumDayValueIn: 0,
      sumDayValueOut: 0,
      food: 0,
      friendandlover: 0,
      gas: 0,
      family: 0,
      children: 0,
      travel: 0,
      pets: 0,
      education: 0,
      book: 0,
      entertainment: 0,
      medical: 0,
      peopleborrow: 0
    };
  }

  reloadData = () => {
    const sumDayValueIn = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year
    };
    const sumDayValueFood = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "food"
    };
    const sumDayValueFriendAndLover = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "friendandlover"
    };
    const sumDayValueGas = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "gas"
    };
    const sumDayValueFamily = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "family"
    };
    const sumDayValueChildren = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "children"
    };
    const sumDayValueTravel = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "travel"
    };
    const sumDayValueMedical = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "medical"
    };
    const sumDayValuePets = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "pets"
    };
    const sumDayValueEducation = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "education"
    };
    const sumDayValueBook = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "book"
    };
    const sumDayValueEntertaiment = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "entertainment"
    };
    const sumDayValuePeopleborrow = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
      shortDescription: "peopleborrow"
    };
    sumDayMoneyValueIN(sumDayValueIn)
      .then(sumDayValueIn => this.setState({ sumDayValueIn }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueFood)
      .then(food => this.setState({ food }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueFriendAndLover)
      .then(friendandlover => this.setState({ friendandlover }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueGas)
      .then(gas => this.setState({ gas }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueFamily)
      .then(family => this.setState({ family }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueChildren)
      .then(children => this.setState({ children }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueTravel)
      .then(travel => this.setState({ travel }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueMedical)
      .then(medical => this.setState({ medical }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValuePets)
      .then(pets => this.setState({ pets }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueEducation)
      .then(education => this.setState({ education }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueBook)
      .then(book => this.setState({ book }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValueEntertaiment)
      .then(entertainment => this.setState({ entertainment }))
      .catch(error => alert(error));
    sumDayMoneyValueOUT(sumDayValuePeopleborrow)
      .then(peopleborrow => this.setState({ peopleborrow }))
      .catch(error => alert(error));
  };
  render() {
    const chart_wh = 200;
    const series = [
      this.state.sumDayValueIn,
      this.state.food,
      this.state.friendandlover,
      this.state.gas,
      this.state.family,
      this.state.children,
      this.state.travel,
      this.state.medical,
      this.state.pets,
      this.state.education,
      this.state.book,
      this.state.entertainment,
      this.state.peopleborrow,
      0.0000000001
    ];
    const sliceColor = [
      "#42eef4",
      "f45341",
      "#e20bbf",
      "#f9b120",
      "#82f232",
      "#750996",
      "#026d2d",
      "#843b00",
      "#005868",
      "#ba0164",
      "#31133d",
      "#ffd9bf",
      "#ff8e77",
      "#e5e5e5"
    ];
    return (
      <View style={styles.container}>
        <View style={{ alignItems: "center" }}>
          <Text style={styles.title}>
            {" "}
            Statistical Money In Day : {this.state.day}/{this.state.month}/
            {this.state.year}{" "}
          </Text>
          <PieChart
            chart_wh={chart_wh}
            series={series}
            sliceColor={sliceColor}
            doughnut={true}
            coverRadius={0.5}
            coverFill={"#e2fff6"}
          />
        </View>
        <Text style={{ fontSize: 20, color: "#42eef4", fontWeight: "bold" }}>
          Today Recive
        </Text>
        <Text style={{ color: "#42eef4", fontWeight: "bold", fontSize: 20 }}>
          {" "}
          + {this.state.sumDayValueIn} VNĐ{" "}
        </Text>
        <ScrollView style={{ flex: 1 }}>
          <Text style={{ fontSize: 10, color: "#f45341", fontWeight: "bold" }}>
            {" "}
            Food Spent{" "}
          </Text>
          <Text style={{ color: "#f45341", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.food} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#e20bbf", fontWeight: "bold" }}>
            {" "}
            Friend and lover Spent{" "}
          </Text>
          <Text style={{ color: "#e20bbf", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.friendandlover} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#f9b120", fontWeight: "bold" }}>
            {" "}
            Gas Spent{" "}
          </Text>
          <Text style={{ color: "#f9b120", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.gas} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#82f232", fontWeight: "bold" }}>
            {" "}
            Family Spent{" "}
          </Text>
          <Text style={{ color: "#82f232", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.family} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#750996", fontWeight: "bold" }}>
            {" "}
            Children Spent{" "}
          </Text>
          <Text style={{ color: "#750996", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.children} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#026d2d", fontWeight: "bold" }}>
            {" "}
            Travel Spent{" "}
          </Text>
          <Text style={{ color: "#026d2d", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.travel} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#843b00", fontWeight: "bold" }}>
            {" "}
            Medical Spent{" "}
          </Text>
          <Text style={{ color: "#843b00", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.medical} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#005868", fontWeight: "bold" }}>
            {" "}
            Pets Spent{" "}
          </Text>
          <Text style={{ color: "#005868", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.pets} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#ba0164", fontWeight: "bold" }}>
            {" "}
            Education Spent{" "}
          </Text>
          <Text style={{ color: "#ba0164", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.education} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#31133d", fontWeight: "bold" }}>
            {" "}
            Book Spent{" "}
          </Text>
          <Text style={{ color: "#31133d", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.book} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#ffd9bf", fontWeight: "bold" }}>
            {" "}
            Entertainment Spent{" "}
          </Text>
          <Text style={{ color: "#ffd9bf", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.entertainment} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#ff8e77", fontWeight: "bold" }}>
            {" "}
            People borrow Spent{" "}
          </Text>
          <Text style={{ color: "#ff8e77", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.peopleborrow} VNĐ
          </Text>
        </ScrollView>
        <ActionButton
          buttonColor="#a6a6a6"
          icon={<Icon name="calendar" size={25} />}
          onPress={() => {
            this.setState({
              scaleAnimationDialog: true
            });
          }}
        />
        <Dialog
          onTouchOutside={() => {
            this.setState({ scaleAnimationDialog: false });
          }}
          width={0.9}
          height={600}
          visible={this.state.scaleAnimationDialog}
          dialogAnimation={new ScaleAnimation()}
          dialogTitle={
            <DialogTitle title=" Calender Select " hasTitleBar={false} />
          }
          actions={[
            <View>
              <DialogButton
                text="OK"
                style={{
                  paddingLeft: 5,
                  flexDirection: "column",
                  borderWidth: 1.0,
                  borderColor: "#d6d7da"
                }}
                textStyle={{ color: "red" }}
                onPress={() => {
                  this.reloadData();
                  this.setState({ scaleAnimationDialog: false });
                }}
                key="button-1"
              />
              <DialogButton
                text="cancel"
                style={{
                  paddingTop: 10,
                  paddingLeft: 5,
                  flexDirection: "column",
                  borderWidth: 1.0,
                  borderColor: "#d6d7da"
                }}
                textStyle={{ color: "#3385ff" }}
                onPress={() => {
                  this.setState({ scaleAnimationDialog: false });
                }}
                key="button-1"
              />
            </View>
          ]}
        >
          <DialogContent />
          <Calendar
            onDayPress={day => {
              this.setState({
                selected: day.dateString,
                year:
                  day.dateString !== undefined
                    ? [day.dateString.split("-")[0].trim()]
                    : "",
                month:
                  day.dateString !== undefined
                    ? day.dateString.split("-")[1].trim()
                    : "",
                day:
                  day.dateString !== undefined
                    ? day.dateString.split("-")[2].trim()
                    : ""
              }),
                console.log(day.dateString);
              console.log(this.state.selected);
            }}
            style={styles.calendar}
            hideExtraDays
            markedDates={{
              [this.state.selected]: {
                selected: true,
                disableTouchEvent: true,
                selectedDotColor: "orange"
              }
            }}
          />
        </Dialog>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  calendar: {},
  text: {
    textAlign: "center",
    borderColor: "#bbb",
    padding: 10,
    backgroundColor: "#eee"
  },
  container: {
    flex: 1,
    backgroundColor: "gray"
  }
});
