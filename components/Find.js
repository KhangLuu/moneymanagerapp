import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList, Picker } from "react-native";
import ActionButton from "react-native-action-button";
import { material } from "react-native-typography";
import Icon from "react-native-vector-icons/FontAwesome";

import Dialog, {
  DialogContent,
  ScaleAnimation,
  DialogButton,
  DialogTitle
} from "react-native-popup-dialog";
import realm from "../databases/allSchemas";
import { selectionDay } from "../databases/allSchemas";
let FlatListItem = props => {
  const {
    itemIndex,
    valuesIN,
    valuesOUT,
    description,
    shortDescription,
    day,
    month,
    year
  } = props;
  return (
    <View
      style={{
        backgroundColor: itemIndex % 2 == 0 ? "#e6e6e6" : "#cccccc",
        borderRadius: 10
      }}
    >
      <Text style={material.subheading}> + Description :{description}</Text>

      <Text style={material.subheading}>
        {" "}
        + Tag expense :{shortDescription}
      </Text>
      <Text
        style={{ fontSize: 15, margin: 2, color: "#1a75ff", paddingLeft: 12 }}
        numberOfLines={1}
      >
        Recevie: {valuesIN} VNĐ
      </Text>
      <Text
        style={{ fontSize: 15, margin: 2, color: "#ff6666", paddingLeft: 12 }}
        numberOfLines={1}
      >
        Spent: {valuesOUT} VNĐ
      </Text>
      <Text
        style={{
          fontWeight: "normal",
          fontSize: 20,
          margin: 2,
          paddingLeft: 270
        }}
        numberOfLines={2}
      >
        Date:{day}/{month}/{year}
      </Text>
    </View>
  );
};
export default class Find extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scaleAnimationDialog: false,
      values: null,
      day: null,
      month: null,
      year: null,
      id: null,
      description: "",
      isAddNew: true,
      allValue: [],
      sumMoney: null,
      sumValueIn: null,
      sumValueOut: null,
      pickerSelection: ""
    };
  }
  reloadData = () => {
    const selection = {
      shortDescription: this.state.pickerSelection
    };
    selectionDay(selection)
      .then(allValue => {
        this.setState({ allValue });
      })
      .catch(error => {
        this.setState({ allValue: [] });
      });
  };
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.flatList}
          data={this.state.allValue}
          renderItem={({ item, index }) => (
            <FlatListItem
              {...item}
              itemIndex={index}
              onPressItem={() => {
                alert(`You pressed item `);
              }}
            />
          )}
          keyExtractor={item => item.id}
        />
        <ActionButton
          buttonColor="#a6a6a6"
          icon={<Icon name="search" size={25} />}
          onPress={() => {
            this.setState({
              scaleAnimationDialog: true
            });
          }}
        />
        <Dialog
          onTouchOutside={() => {
            this.setState({ scaleAnimationDialog: false });
          }}
          width={0.9}
          height={200}
          visible={this.state.scaleAnimationDialog}
          dialogAnimation={new ScaleAnimation()}
          dialogTitle={
            <DialogTitle title=" Search Tag  " hasTitleBar={false} />
          }
          actions={[
            <View>
              <View
                style={{
                  flexDirection: "column",
                  borderWidth: 1.0,
                  borderColor: "#d6d7da"
                }}
              >
                <DialogButton
                  text="OK"
                  textStyle={{ color: "red" }}
                  onPress={() => {
                    this.reloadData();
                    this.setState({ scaleAnimationDialog: false });
                  }}
                  key="button-1"
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  borderWidth: 1.0,
                  borderColor: "#d6d7da"
                }}
              >
                <DialogButton
                  text="cancel"
                  textStyle={{ color: "#3385ff" }}
                  onPress={() => {
                    this.setState({ scaleAnimationDialog: false });
                  }}
                  key="button-1"
                />
              </View>
            </View>
          ]}
        >
          <DialogContent>
            <Picker
              selectedValue={this.state.pickerSelection}
              style={{ height: 20, width: 200 }}
              mode="dropdown"
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ pickerSelection: itemValue })
              }
            >
              <Picker.Item label="..." value="..." />
              <Picker.Item label="Food" value="food" />
              <Picker.Item label="Friend & Lover" value="friendandlover" />
              <Picker.Item label="Gas" value="gas" />
              <Picker.Item label="Family" value="family" />
              <Picker.Item label="Children" value="children" />
              <Picker.Item label="Travel" value="travel" />
              <Picker.Item label="Pets" value="pets" />
              <Picker.Item label="Education" value="education" />
              <Picker.Item label="Book" value="book" />
              <Picker.Item label="Pets" value="pets" />
              <Picker.Item label="Entertainment" value="entertainment" />
              <Picker.Item label="Medical" value="medical" />
              <Picker.Item label="People borrow" value="peopleborrow" />
            </Picker>
          </DialogContent>
        </Dialog>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "gray"
  },
  message: {
    fontSize: 30
  },
  flatList: {
    flex: 1
  }
});
