import React, { Component } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { TabNavigator, DrawerNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome";
import HomeTab from "./HomeTab";
import TodayTab from "./TodayTab";
import YesterdayTab from "./YesterdayTab";
import MonthTab from "./MonthTab";
import Year from "./Year";
import moneyCalender from "./Calendar";
import Find from "./Find";
import Setting from "./setting";
export default class HomeScreen extends Component {
  static navigationOptions = {
    headerLeft: <Icon name="money" style={{ paddingLeft: 15 }} size={30} />,
    title: "Money manager ",
    headerTitleStyle: {
      color: "fefefe",
      fontFamily: "MuseoSansRounded-300",
      fontWeight: "300",
      justifyContent: "space-between",
      textAlign: "center"
    },
    headerStyle: {
      backgroundColor: "#f2f2f2",
      height: 50
    }
  };
  render() {
    return <SlideMenu />;
  }
}

// Create Tab navigator
export const AppTabNavigator = TabNavigator(
  {
    // Load HomeTab
    HomeTab: {
      screen: HomeTab,
      navigationOptions: {
        tabBarLabel: "HOME",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" style={{ color: tintColor }} size={30} />
        )
      }
    },
    // Load TodayTab
    TodayTab: {
      screen: TodayTab,
      navigationOptions: {
        tabBarLabel: "TODAY",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="angle-double-up" style={{ color: tintColor }} size={30} />
        )
      }
    },
    // Load YesterdayTab
    YesterdayTab: {
      screen: YesterdayTab,
      navigationOptions: {
        tabBarLabel: "Yesterday",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="angle-double-down"
            style={{ color: tintColor }}
            size={30}
          />
        )
      }
    },
    // Load MonthTab
    MonthTab: {
      screen: MonthTab,
      navigationOptions: {
        tabBarLabel: "MONTH",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="th" style={{ color: tintColor }} size={25} />
        )
      }
    }
  },
  // Setting option for all tab
  {
    animationEnabled: true,
    tabBarPosition: "bottom",
    tabBarOptions: {
      style: {
        backgroundColor: "#dfdfdf",
        height: 50
      },
      activeTintColor: "#000000",
      activeBackgroundColor: "#ffffff",
      inactiveTintColor: "#4c4c4c",
      showIcon: true,
      labelStyle: {
        fontSize: 8
      }
    }
  }
);

// Create SlideMenu
export const SlideMenu = DrawerNavigator(
  {
    Home: {
      screen: AppTabNavigator,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="home" style={{ color: tintColor }} size={25} />
        )
      }
    },
    Year: {
      screen: Year,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="globe" style={{ color: tintColor }} size={25} />
        )
      }
    },
    Calendar: {
      screen: moneyCalender,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="calendar" style={{ color: tintColor }} size={25} />
        )
      }
    },
    Find: {
      screen: Find,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="search" style={{ color: tintColor }} size={25} />
        )
      }
    },
    Setting: {
      screen: Setting,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="cog" style={{ color: tintColor }} size={25} />
        )
      }
    }
  },
  // Setting option for all slideMenu
  {
    contentOptions: {
      activeTintColor: "#000000",
      activeBackgroundColor: "#c1c1c1",
      inactiveTintColor: "#5a5a5a"
    },
    drawerWidth: 200,
    drawerPosition: "Left",
    navigationOptions: {
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("DrawerOpen");
          }}
        >
          <Icon name="bars" style={{ paddingLeft: 15 }} size={25} />
        </TouchableOpacity>
      )
    }
  }
);
// Stting style for HomeScreen stack
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
