import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  FlatList,
  Picker
} from "react-native";
import ActionButton from "react-native-action-button";
import { material } from "react-native-typography";
import Icon from "react-native-vector-icons/FontAwesome";

import Dialog, {
  DialogContent,
  ScaleAnimation,
  DialogButton,
  DialogTitle
} from "react-native-popup-dialog";
import realm from "../databases/allSchemas";
import {
  insertNewDay,
  selectionNewDay,
  sumValueIN,
  sumValueOUT
} from "../databases/allSchemas";
let FlatListItem = props => {
  const {
    itemIndex,
    valuesIN,
    valuesOUT,
    description,
    shortDescription,
    day,
    month,
    year
  } = props;
  return (
    <View
      style={{
        backgroundColor: itemIndex % 2 == 0 ? "#e6e6e6" : "#cccccc",
        borderRadius: 10
      }}
    >
      <Text style={material.subheading}> + Description :{description}</Text>
      <Text style={material.subheading}>
        {" "}
        + Tag expense :{shortDescription}
      </Text>
      <Text
        style={{ fontSize: 15, margin: 2, color: "#1a75ff", paddingLeft: 12 }}
        numberOfLines={1}
      >
        Recevie: {valuesIN} VNĐ
      </Text>
      <Text
        style={{ fontSize: 15, margin: 2, color: "#ff6666", paddingLeft: 12 }}
        numberOfLines={1}
      >
        Spent: {valuesOUT} VNĐ
      </Text>
      <Text
        style={{
          fontWeight: "normal",
          fontSize: 20,
          margin: 2,
          paddingLeft: 270
        }}
        numberOfLines={2}
      >
        Date:{day}/{month}/{year}
      </Text>
    </View>
  );
};
export default class HomeTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scaleAnimationDialog: false,
      values: null,
      day: null,
      month: null,
      year: null,
      id: null,
      description: "",
      isAddNew: true,
      allValue: [],
      sumMoney: null,
      sumValueIn: null,
      sumValueOut: null,
      pickerSelection: "food"
    };
    this.reloadData();
    realm.addListener("change", () => {
      this.reloadData();
    });
  }
  reloadData = () => {
    sumValueIN()
      .then(sumValueIn => this.setState({ sumValueIn }))
      .catch(error => alert(error));
    sumValueOUT()
      .then(sumValueOut => this.setState({ sumValueOut }))
      .catch(error => alert(error));
    selectionNewDay()
      .then(allValue => {
        this.setState({ allValue });
      })
      .catch(error => {
        this.setState({ allValue: [] });
      });
  };
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.flatList}
          data={this.state.allValue}
          renderItem={({ item, index }) => (
            <FlatListItem
              {...item}
              itemIndex={index}
              onPressItem={() => {
                alert(`You pressed item `);
              }}
            />
          )}
          keyExtractor={item => item.id}
        />
        <View style={{ borderRadius: 40, paddingLeft: 20 }}>
          <Text style={material.captionObject}> Wallet :</Text>
          <Icon name="credit-card" size={15} />
          <Text style={material.display1Object}>
            {this.state.sumValueIn - this.state.sumValueOut} VNĐ
          </Text>
        </View>
        <ActionButton
          buttonColor="#a6a6a6"
          onPress={() => {
            this.setState({
              scaleAnimationDialog: true
            });
          }}
        />
        <Dialog
          onTouchOutside={() => {
            this.setState({ scaleAnimationDialog: false });
          }}
          width={0.9}
          height={300}
          style={{ flexDirection: "row" }}
          visible={this.state.scaleAnimationDialog}
          dialogAnimation={new ScaleAnimation()}
          dialogTitle={
            <DialogTitle title=" Money Input " hasTitleBar={false} />
          }
          actions={[
            <View>
              <DialogButton
                text="Spent"
                textStyle={{
                  color: "red",
                  flexDirection: "column",
                  borderWidth: 1.0,
                  borderColor: "#d6d7da"
                }}
                onPress={() => {
                  if (this.state.values == "") {
                    alert("plese enter value");
                    return;
                  } else {
                    if (this.state.isAddNew == true) {
                      const newMoney = {
                        id: Math.floor(Date.now() / 1000),
                        day: new Date().getDate(),
                        month: new Date().getMonth() + 1,
                        year: new Date().getFullYear(),
                        valuesOUT: Number(this.state.values),
                        valuesIN: 0,
                        description: this.state.description,
                        shortDescription: this.state.pickerSelection
                      };
                      insertNewDay(newMoney)
                        .then()
                        .catch(error => alert(error));
                    } else {
                    }
                  }
                  this.setState({
                    scaleAnimationDialog: false
                  });
                  this.setState({ values: "", description: "" });
                }}
                key="button-1"
              />
              <DialogButton
                text="Recevie"
                textStyle={{
                  color: "#3385ff",
                  flexDirection: "column",
                  borderWidth: 1.0,
                  borderColor: "#d6d7da"
                }}
                onPress={() => {
                  if (this.state.values == "") {
                    alert("plese enter value");
                    return;
                  } else {
                    if (this.state.isAddNew == true) {
                      const newMoney = {
                        id: Math.floor(Date.now() / 1000),
                        day: new Date().getDate(),
                        month: new Date().getMonth() + 1,
                        year: new Date().getFullYear(),
                        valuesIN: Number(this.state.values),
                        valuesOUT: 0,
                        description: this.state.description,
                        shortDescription: "moneyrecevie"
                      };

                      insertNewDay(newMoney)
                        .then()
                        .catch(error => alert(error));
                    } else {
                    }
                  }

                  this.setState({
                    scaleAnimationDialog: false
                  });
                  this.setState({ values: "", description: "" });
                }}
                key="button-1"
              />
            </View>
          ]}
        >
          <DialogContent>
            <TextInput
              placeholder="Enter Value VNĐ Here  "
              keyboardType="numeric"
              ref={input => {
                this.TextInput;
              }}
              onChangeText={values => this.setState({ values })}
              value={this.state.values}
            />
            <TextInput
              placeholder="Description"
              ref={input => {
                this.TextInput;
              }}
              onChangeText={description => this.setState({ description })}
              value={this.state.description}
            />
            <Text style={{ fontSize: 15 }}>Tag Spent Group</Text>
            <Picker
              selectedValue={this.state.pickerSelection}
              style={{ height: 20, width: 200 }}
              mode="dropdown"
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ pickerSelection: itemValue })
              }
            >
              <Picker.Item label="Food" value="food" />
              <Picker.Item label="Friend & Lover" value="friendandlover" />
              <Picker.Item label="Gas" value="gas" />
              <Picker.Item label="Family" value="family" />
              <Picker.Item label="Children" value="children" />
              <Picker.Item label="Travel" value="travel" />
              <Picker.Item label="Pets" value="pets" />
              <Picker.Item label="Education" value="education" />
              <Picker.Item label="Book" value="book" />
              <Picker.Item label="Pets" value="pets" />
              <Picker.Item label="Entertainment" value="entertainment" />
              <Picker.Item label="Medical" value="medical" />
              <Picker.Item label="People borrow" value="peopleborrow" />
            </Picker>
          </DialogContent>
        </Dialog>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "gray"
  },
  message: {
    fontSize: 30
  },
  flatList: {
    flex: 1
  }
});
