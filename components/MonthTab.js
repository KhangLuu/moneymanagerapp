import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, ScrollView } from "react-native";
import {
  sumMonthMoneyValueIN,
  sumMonthMoneyValueOUT
} from "../databases/allSchemas";
import realm from "../databases/allSchemas";
import PieChart from "react-native-pie-chart";
class MonthTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sumMonthValueIn: 0,
      sumMonthValueOut: 0,
      food: 0,
      friendandlover: 0,
      gas: 0,
      family: 0,
      children: 0,
      travel: 0,
      pets: 0,
      education: 0,
      book: 0,
      entertainment: 0,
      medical: 0,
      peopleborrow: 0
    };
    this.reloadData();
    realm.addListener("change", () => this.reloadData());
  }

  reloadData = () => {
    const sumMonthValueIn = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear()
    };
    const sumDayValueFood = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "food"
    };
    const sumDayValueFriendAndLover = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "friendandlover"
    };
    const sumDayValueGas = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "gas"
    };
    const sumDayValueFamily = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "family"
    };
    const sumDayValueChildren = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "children"
    };
    const sumDayValueTravel = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "travel"
    };
    const sumDayValueMedical = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "medical"
    };
    const sumDayValuePets = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "pets"
    };
    const sumDayValueEducation = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "education"
    };
    const sumDayValueBook = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "book"
    };
    const sumDayValueEntertaiment = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "entertainment"
    };
    const sumDayValuePeopleborrow = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      shortDescription: "peopleborrow"
    };
    sumMonthMoneyValueIN(sumMonthValueIn)
      .then(sumMonthValueIn => this.setState({ sumMonthValueIn }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueFood)
      .then(food => this.setState({ food }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueFriendAndLover)
      .then(friendandlover => this.setState({ friendandlover }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueGas)
      .then(gas => this.setState({ gas }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueFamily)
      .then(family => this.setState({ family }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueChildren)
      .then(children => this.setState({ children }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueTravel)
      .then(travel => this.setState({ travel }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueMedical)
      .then(medical => this.setState({ medical }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValuePets)
      .then(pets => this.setState({ pets }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueEducation)
      .then(education => this.setState({ education }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueBook)
      .then(book => this.setState({ book }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValueEntertaiment)
      .then(entertainment => this.setState({ entertainment }))
      .catch(error => alert(error));
    sumMonthMoneyValueOUT(sumDayValuePeopleborrow)
      .then(peopleborrow => this.setState({ peopleborrow }))
      .catch(error => alert(error));
  };
  render() {
    const chart_wh = 200;
    const series = [
      this.state.sumMonthValueIn,
      this.state.food,
      this.state.friendandlover,
      this.state.gas,
      this.state.family,
      this.state.children,
      this.state.travel,
      this.state.medical,
      this.state.pets,
      this.state.education,
      this.state.book,
      this.state.entertainment,
      this.state.peopleborrow,
      0.0000000001
    ];
    const sliceColor = [
      "#42eef4",
      "f45341",
      "#e20bbf",
      "#f9b120",
      "#82f232",
      "#750996",
      "#026d2d",
      "#843b00",
      "#005868",
      "#ba0164",
      "#31133d",
      "#ffd9bf",
      "#ff8e77",
      "#e5e5e5"
    ];

    return (
      <View style={styles.container}>
        <View style={{ alignItems: "center" }}>
          <Text style={styles.title}> Statistical Money In Month </Text>
          <PieChart
            chart_wh={chart_wh}
            series={series}
            sliceColor={sliceColor}
            doughnut={true}
            coverRadius={0.5}
            coverFill={"#e2fff6"}
          />
        </View>
        <Text style={{ fontSize: 20, color: "#42eef4", fontWeight: "bold" }}>
          Month Recive
        </Text>
        <Text style={{ color: "#42eef4", fontWeight: "bold", fontSize: 20 }}>
          {" "}
          + {this.state.sumMonthValueIn} VNĐ{" "}
        </Text>
        <ScrollView style={{ flex: 1 }}>
          <Text style={{ fontSize: 10, color: "#f45341", fontWeight: "bold" }}>
            {" "}
            Food Spent{" "}
          </Text>
          <Text style={{ color: "#f45341", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.food} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#e20bbf", fontWeight: "bold" }}>
            {" "}
            Friend and lover Spent{" "}
          </Text>
          <Text style={{ color: "#e20bbf", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.friendandlover} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#f9b120", fontWeight: "bold" }}>
            {" "}
            Gas Spent{" "}
          </Text>
          <Text style={{ color: "#f9b120", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.gas} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#82f232", fontWeight: "bold" }}>
            {" "}
            Family Spent{" "}
          </Text>
          <Text style={{ color: "#82f232", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.family} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#750996", fontWeight: "bold" }}>
            {" "}
            Children Spent{" "}
          </Text>
          <Text style={{ color: "#750996", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.children} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#026d2d", fontWeight: "bold" }}>
            {" "}
            Travel Spent{" "}
          </Text>
          <Text style={{ color: "#026d2d", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.travel} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#843b00", fontWeight: "bold" }}>
            {" "}
            Medical Spent{" "}
          </Text>
          <Text style={{ color: "#843b00", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.medical} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#005868", fontWeight: "bold" }}>
            {" "}
            Pets Spent{" "}
          </Text>
          <Text style={{ color: "#005868", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.pets} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#ba0164", fontWeight: "bold" }}>
            {" "}
            Education Spent{" "}
          </Text>
          <Text style={{ color: "#ba0164", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.education} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#31133d", fontWeight: "bold" }}>
            {" "}
            Book Spent{" "}
          </Text>
          <Text style={{ color: "#31133d", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.book} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#ffd9bf", fontWeight: "bold" }}>
            {" "}
            Entertainment Spent{" "}
          </Text>
          <Text style={{ color: "#ffd9bf", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.entertainment} VNĐ
          </Text>
          <Text style={{ fontSize: 10, color: "#ff8e77", fontWeight: "bold" }}>
            {" "}
            People borrow Spent{" "}
          </Text>
          <Text style={{ color: "#ff8e77", fontWeight: "bold", fontSize: 15 }}>
            -{this.state.peopleborrow} VNĐ
          </Text>
        </ScrollView>
      </View>
    );
  }
}
export default MonthTab;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 5,
    backgroundColor: "#e8ebed"
  },
  title: {
    fontSize: 24,
    margin: 10,
    color: "black"
  }
});
