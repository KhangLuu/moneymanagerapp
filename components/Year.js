import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions, ScrollView } from "react-native";
import { material } from "react-native-typography";
import { LineChart } from "react-native-chart-kit";
import {
  sumMonthMoneyValueIN,
  sumMonthMoneyOUT
} from "../databases/allSchemas";
import realm from "../databases/allSchemas";
const screenWidth = Dimensions.get("window").width;
const chartConfig = {
  backgroundColor: "#40dbdb",
  backgroundGradientFrom: "#92c9ed",
  backgroundGradientTo: "#ff0040",
  decimalPlaces: 2, // optional, defaults to 2dp
  textSize: 2,
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
};
export default class Year extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sumMonthValueIn: null,
      sumMonthValueOut: null,
      sumInJan: 0,
      sumOutJan: 0,
      sumInFeb: 0,
      sumOutFeb: 0,
      sumInMar: 0,
      sumOutMar: 0,
      sumInArp: 0,
      sumOutArp: 0,
      sumInMay: 0,
      sumOutMay: 0,
      sumInJun: 0,
      sumOutJun: 0,
      sumInJul: 0,
      sumOutJul: 0,
      sumInAug: 0,
      sumOutAug: 0,
      sumInSep: 0,
      sumOutSep: 0,
      sumInOct: 0,
      sumOutOct: 0,
      sumInNov: 0,
      sumOutNov: 0,
      sumInDec: 0,
      sumOutDec: 0
    };
    this.reloadData();
    realm.addListener("change", () => this.reloadData());
  }
  reloadData = () => {
    const month1 = {
      month: "1",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month1)
      .then(sumInJan => this.setState({ sumInJan }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month1)
      .then(sumOutJan => this.setState({ sumOutJan }))
      .catch(error => alert(error));
    const month2 = {
      month: "2",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month2)
      .then(sumInFeb => this.setState({ sumInFeb }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month2)
      .then(sumOutFeb => this.setState({ sumOutFeb }))
      .catch(error => alert(error));
    const month3 = {
      month: "3",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month3)
      .then(sumInMar => this.setState({ sumInMar }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month3)
      .then(sumOutMar => this.setState({ sumOutMar }))
      .catch(error => alert(error));
    const month4 = {
      month: "4",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month4)
      .then(sumInArp => this.setState({ sumInArp }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month4)
      .then(sumOutArp => this.setState({ sumOutArp }))
      .catch(error => alert(error));
    const month5 = {
      month: "5",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month5)
      .then(sumInMay => this.setState({ sumInMay }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month5)
      .then(sumOutMay => this.setState({ sumOutMay }))
      .catch(error => alert(error));
    const month6 = {
      month: "6",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month6)
      .then(sumInJun => this.setState({ sumInJun }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month6)
      .then(sumOutJun => this.setState({ sumOutJun }))
      .catch(error => alert(error));
    const month7 = {
      month: "7",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month7)
      .then(sumInJul => this.setState({ sumInJul }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month7)
      .then(sumOutJul => this.setState({ sumOutJul }))
      .catch(error => alert(error));
    const month8 = {
      month: "8",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month8)
      .then(sumInAug => this.setState({ sumInAug }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month8)
      .then(sumOutAug => this.setState({ sumOutAug }))
      .catch(error => alert(error));
    const month9 = {
      month: "9",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month9)
      .then(sumInSep => this.setState({ sumInSep }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month9)
      .then(sumOutSep => this.setState({ sumOutSep }))
      .catch(error => alert(error));
    const month10 = {
      month: "10",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month10)
      .then(sumInOct => this.setState({ sumInOct }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month10)
      .then(sumOutOct => this.setState({ sumOutOct }))
      .catch(error => alert(error));
    const month11 = {
      month: "11",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month11)
      .then(sumInNov => this.setState({ sumInNov }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month11)
      .then(sumOutNov => this.setState({ sumOutNov }))
      .catch(error => alert(error));
    const month12 = {
      month: "12",
      year: new Date().getFullYear()
    };
    sumMonthMoneyValueIN(month12)
      .then(sumInDec => this.setState({ sumInDec }))
      .catch(error => alert(error));
    sumMonthMoneyOUT(month12)
      .then(sumOutDec => this.setState({ sumOutDec }))
      .catch(error => alert(error));
  };
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ borderRadius: 20 }}>
            <Text style={material.body1}>
              {" "}
              Month Statistic recive (Value x 1000000 VNĐ){" "}
            </Text>
            <LineChart
              data={{
                labels: [
                  "Jan",
                  "Feb",
                  "Mar",
                  "Apr",
                  "May",
                  "Jun",
                  "Jul",
                  "Aug",
                  "Sep",
                  "oct",
                  "Nov",
                  "Dec"
                ],
                datasets: [
                  {
                    data: [
                      this.state.sumInJan / 1000000,
                      this.state.sumInFeb / 1000000,
                      this.state.sumInMar / 1000000,
                      this.state.sumInArp / 1000000,
                      this.state.sumInMay / 1000000,
                      this.state.sumInJun / 1000000,
                      this.state.sumInJul / 1000000,
                      this.state.sumInAug / 1000000,
                      this.state.sumInSep / 1000000,
                      this.state.sumInOct / 1000000,
                      this.state.sumInNov / 1000000,
                      this.state.sumInDec / 1000000
                    ]
                  }
                ]
              }}
              width={screenWidth}
              height={220}
              chartConfig={chartConfig}
              bezier
            />
            <View />
            <View>
              <Text style={material.body1}>
                {" "}
                Month Statistic Spent(Value x 1000000 VNĐ){" "}
              </Text>
              <LineChart
                data={{
                  labels: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "oct",
                    "Nov",
                    "Dec"
                  ],
                  datasets: [
                    {
                      data: [
                        this.state.sumOutJan / 1000000,
                        this.state.sumOutFeb / 1000000,
                        this.state.sumOutMar / 1000000,
                        this.state.sumOutArp / 1000000,
                        this.state.sumOutMay / 1000000,
                        this.state.sumOutJun / 1000000,
                        this.state.sumOutJul / 1000000,
                        this.state.sumOutAug / 1000000,
                        this.state.sumOutSep / 1000000,
                        this.state.sumOutOct / 1000000,
                        this.state.sumOutNov / 1000000,
                        this.state.sumOutDec / 1000000
                      ]
                    }
                  ]
                }}
                width={screenWidth}
                height={220}
                chartConfig={chartConfig}
                bezier
              />
            </View>
            <View>
              <Text style={{ color: "#42eef4" }}>
                {" "}
                Sum Money Recive in Year
              </Text>
              <Text style={{ color: "#42eef4" }}>
                +{" "}
                {this.state.sumInJan +
                  this.state.sumInFeb +
                  this.state.sumInMar +
                  this.state.sumInArp +
                  this.state.sumInMay +
                  this.state.sumInJun +
                  this.state.sumInJul +
                  this.state.sumInAug +
                  this.state.sumInSep +
                  this.state.sumInOct +
                  this.state.sumInNov +
                  this.state.sumInDec}{" "}
                VNĐ
              </Text>
              <Text style={{ color: "#f45341" }}> Sum Money Spent in Year</Text>
              <Text style={{ color: "#f45341" }}>
                -{" "}
                {this.state.sumOutJan +
                  this.state.sumOutFeb +
                  this.state.sumOutMar +
                  this.state.sumOutArp +
                  this.state.sumOutMay +
                  this.state.sumOutJun +
                  this.state.sumOutJul +
                  this.state.sumOutAug +
                  this.state.sumOutSep +
                  this.state.sumOutOct +
                  this.state.sumOutNov +
                  this.state.sumOutDec}{" "}
                VNĐ
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#dfe2de"
  },
  chart: {
    flex: 1
  }
});
