import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import { BarChart } from "react-native-chart-kit";
import {
  sumYesterdayMoneyValueIN,
  sumYesterdayMoneyValueOUT
} from "../databases/allSchemas";
import realm from "../databases/allSchemas";
const screenWidth = Dimensions.get("window").width;
const chartConfig = {
  backgroundColor: "#40dbdb",
  backgroundGradientFrom: "#92c9ed",
  backgroundGradientTo: "#ff0040",
  decimalPlaces: 2, // optional, defaults to 2dp
  textSize: 2,
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
};

export default class YesterdayTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sumYesterdayValueIn: null,
      sumYesterdayValueOut: null,
      day: parseInt(new Date().getDate() - 1),
      month: parseInt(new Date().getMonth() + 1),
      year: parseInt(new Date().getFullYear())
    };
    this.checkDay();
    this.reloadData();
    realm.addListener("change", () => {
      this.reloadData(), this.checkDay();
    });
  }
  reloadData = () => {
    const date = {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year
    };
    sumYesterdayMoneyValueIN(date)
      .then(sumYesterdayValueIn => this.setState({ sumYesterdayValueIn }))
      .catch(error => alert(error));
    sumYesterdayMoneyValueOUT(date)
      .then(sumYesterdayValueOut => this.setState({ sumYesterdayValueOut }))
      .catch(error => alert(error));
  };
  checkDay = () => {
    if (this.state.day == 0 && this.state.month - 1 == 0) {
      this.state.day = 31;
      this.state.month = 12;
      this.state.year = this.state.year - 1;
    }
    if (
      this.state.day == 0 &&
      this.state.month - 1 != 0 &&
      (this.state.month == 5 ||
        this.state.month == 7 ||
        this.state.month == 8 ||
        this.state.month == 10 ||
        this.state.month == 12)
    ) {
      this.state.day = 30;
      this.state.month = this.state.month - 1;
    }
    if (
      this.state.day == 0 &&
      this.state.month - 1 != 0 &&
      (this.state.month == 2 ||
        this.state.month == 4 ||
        this.state.month == 6 ||
        this.state.month == 9 ||
        this.state.month == 11)
    ) {
      this.state.day = 31;
      this.state.month = this.state.month - 1;
    }
    if (this.state.day == 0 && this.month == 3 && this.month - 1 != 0) {
      if (this.state.year % 4 == 0 && this.state.year % 8 == 0) {
        this.state.day = 29;
        this.state.month = this.state.month - 1;
      } else {
        this.state.day = 28;
        this.state.month = this.state.month - 1;
      }
    }
  };

  render() {
    const data = {
      labels: ["Recivce", "Spent"],
      datasets: [
        {
          data: [
            this.state.sumYesterdayValueIn / 100000,
            this.state.sumYesterdayValueOut / 100000
          ]
        }
      ]
    };
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 30, color: "#42eef4", fontWeight: "bold" }}>
          Yesterday receive{" "}
        </Text>
        <Text style={{ color: "#42eef4", fontWeight: "bold", fontSize: 20 }}>
          + {this.state.sumYesterdayValueIn} VNĐ
        </Text>
        <Text style={{ fontSize: 30, color: "#f45341", fontWeight: "bold" }}>
          Yesterday Spent{" "}
        </Text>
        <Text style={{ color: "#f45341", fontWeight: "bold", fontSize: 20 }}>
          - {this.state.sumYesterdayValueOut} VNĐ
        </Text>
        <View>
          <Text style={{ fontSize: 20, fontWeight: "bold", color: "#ffffff" }}>
            Statistical Money In Yesterday (value* 100000 VNĐ)
          </Text>
          <BarChart
            data={data}
            width={screenWidth}
            height={220}
            chartConfig={chartConfig}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "gray",
    paddingLeft: 5
  }
});
