import React, { Component } from "react";
import { View, Text, StyleSheet, AsyncStorage, Switch } from "react-native";
import PushNotification from "react-native-push-notification";
const key = "@MyApp:key";
class Setting extends Component {
  showNoti() {
    PushNotification.localNotification({
      id: "0",
      message: " Click here to quick entry Wallet ",
      title: " Money Manager ",
      playSound: true,
      ongoing: true,
      vibrate: true,
      color: "red",
      autoCancel: false
    });
  }
  cancelNoti() {
    PushNotification.cancelAllLocalNotifications({
      id: "0"
    });
  }
  state = {
    isNotifi: false
  };
  componentDidMount() {
    this.onLoad();
  }
  onSave = async () => {
    try {
      await AsyncStorage.setItem(key, (!this.state.isNotifi).toString());
    } catch (error) {}
  };
  load = () => {
    this.setState({ isNotifi: JSON.parse(this.state.isNotifi) });
  };
  onLoad = async () => {
    try {
      this.setState({
        isNotifi: await AsyncStorage.getItem(key)
      });
      this.load();
    } catch (error) {}
  };
  onChange = () => {
    this.setState({ isNotifi: !this.state.isNotifi });
    if (!this.state.isNotifi) {
      this.showNoti();
    } else {
      this.cancelNoti();
    }
    this.onSave();
  };
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Quick entry wallet </Text>
        <Switch onValueChange={this.onChange} value={this.state.isNotifi} />
      </View>
    );
  }
}

export default Setting;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "gray"
  },
  text: {
    color: "black",
    alignItems: "center",
    fontSize: 20,
    paddingLeft: 20,
    paddingTop: 30
  }
});
