import Realm, { schemaVersion } from "realm";
export const DAY_MONEY = "daymoney";
export const daymoney = {
  name: DAY_MONEY,
  primaryKey: "id",
  properties: {
    id: "int",
    valuesIN: "int",
    valuesOUT: "int",
    description: "string",
    shortDescription: "string",
    day: "int",
    month: "int",
    year: "int"
  }
};
const databaseOptions = {
  path: "todoListApp.realm",
  schema: [daymoney],
  schemaVersion: 0
};
export const insertNewDay = newDay =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        realm.write(() => {
          realm.create(DAY_MONEY, newDay);
          resolve(insertNewDay);
        });
      })
      .catch(error => reject(error));
  });
export const selectionNewDay = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let selectionNewDay = realm.objects(DAY_MONEY);
        selectionNewDay = selectionNewDay.sorted("id", (ascending = true));
        resolve(selectionNewDay);
      })
      .catch(error => reject(error));
  });
export const selectionDay = selection =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let selectionDay = realm
          .objects(DAY_MONEY)
          .filtered("shortDescription = '" + selection.shortDescription + "'");
        selectionDay = selectionDay.sorted("id", (ascending = true));
        resolve(selectionDay);
      })
      .catch(error => reject(error));
  });
export const sumDayMoneyValueIN = sumDayValueIn =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumDayMoneyValueIN = realm
          .objects(DAY_MONEY)
          .filtered(
            "day = " +
              sumDayValueIn.day +
              "AND  month = " +
              sumDayValueIn.month +
              "AND year = " +
              sumDayValueIn.year
          );
        sumDayMoneyValueIN = sumDayMoneyValueIN.sum("valuesIN");
        resolve(sumDayMoneyValueIN);
      })
      .catch(error => reject(error));
  });
export const sumDayMoneyValueOUT = sumDayValueOut =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumDayMoneyValueOUT = realm
          .objects(DAY_MONEY)
          .filtered(
            "day = " +
              sumDayValueOut.day +
              "AND  month = " +
              sumDayValueOut.month +
              "AND year = " +
              sumDayValueOut.year +
              "AND shortDescription = '" +
              sumDayValueOut.shortDescription +
              "'"
          );
        sumDayMoneyValueOUT = sumDayMoneyValueOUT.sum("valuesOUT");
        resolve(sumDayMoneyValueOUT);
      })
      .catch(error => reject(error));
  });
export const sumValueOUT = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumValueOUT = realm.objects(DAY_MONEY);
        sumValueOUT = sumValueOUT.sum("valuesOUT");
        resolve(sumValueOUT);
      })
      .catch(error => reject(error));
  });
export const sumValueIN = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumValueIN = realm.objects(DAY_MONEY);
        sumValueIN = sumValueIN.sum("valuesIN");
        resolve(sumValueIN);
      })
      .catch(error => reject(error));
  });
export const sumYesterdayMoneyValueIN = sumYesterdayValueIn =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumYesterdayMoneyValueIN = realm
          .objects(DAY_MONEY)
          .filtered(
            "day = " +
              sumYesterdayValueIn.day +
              "AND  month = " +
              sumYesterdayValueIn.month +
              "AND year = " +
              sumYesterdayValueIn.year
          );
        sumYesterdayMoneyValueIN = sumYesterdayMoneyValueIN.sum("valuesIN");
        resolve(sumYesterdayMoneyValueIN);
      })
      .catch(error => reject(error));
  });
export const sumYesterdayMoneyValueOUT = sumYesterdayValueOut =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumYesterdayMoneyValueOUT = realm
          .objects(DAY_MONEY)
          .filtered(
            "day = " +
              sumYesterdayValueOut.day +
              "AND  month = " +
              sumYesterdayValueOut.month +
              "AND year = " +
              sumYesterdayValueOut.year
          );
        sumYesterdayMoneyValueOUT = sumYesterdayMoneyValueOUT.sum("valuesOUT");
        resolve(sumYesterdayMoneyValueOUT);
      })
      .catch(error => reject(error));
  });
export const sumMonthMoneyValueIN = sumMonthValueIn =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumMonthMoneyValueIN = realm
          .objects(DAY_MONEY)
          .filtered(
            "  month = " +
              sumMonthValueIn.month +
              "AND year = " +
              sumMonthValueIn.year
          );
        sumMonthMoneyValueIN = sumMonthMoneyValueIN.sum("valuesIN");
        resolve(sumMonthMoneyValueIN);
      })
      .catch(error => reject(error));
  });
export const sumMonthMoneyValueOUT = sumMonthValueOut =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumMonthMoneyValueOUT = realm
          .objects(DAY_MONEY)
          .filtered(
            "  month = " +
              sumMonthValueOut.month +
              "AND year = " +
              sumMonthValueOut.year +
              "AND shortDescription = '" +
              sumMonthValueOut.shortDescription +
              "'"
          );
        sumMonthMoneyValueOUT = sumMonthMoneyValueOUT.sum("valuesOUT");
        resolve(sumMonthMoneyValueOUT);
      })
      .catch(error => reject(error));
  });
export const sumMonthMoneyOUT = sumMonthValueOut =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let sumMonthMoneyValueOUT = realm
          .objects(DAY_MONEY)
          .filtered(
            "  month = " +
              sumMonthValueOut.month +
              "AND year = " +
              sumMonthValueOut.year
          );
        sumMonthMoneyValueOUT = sumMonthMoneyValueOUT.sum("valuesOUT");
        resolve(sumMonthMoneyValueOUT);
      })
      .catch(error => reject(error));
  });
export default new Realm(databaseOptions);
